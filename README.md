# Depende

Install dependencies from different sources, like apt or pip.

## Installation

```
pip3 install git+https://gitlab.com/Zer1t0/depende
```

## Example

Define your dependencies in a yaml file:
```yaml
python: [pyyaml, ipython]

apt: [build-essential, git]

download:
  - name: subfinder
    url: https://github.com/projectdiscovery/subfinder/releases/download/v2.5.5/subfinder_2.5.5_linux_amd64.zip
    dest: /usr/bin/subfinder
    unzip: true
```

And use that file with depende:
```
depende requirements.yaml
```

There are some examples of files in the `samples` directory.

## Currently supported

- [X] pip (python3)
- [X] apt
- [X] download file
- [ ] yum
- [ ] go
- [ ] many more...

