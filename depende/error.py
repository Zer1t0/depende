

class InstallationError(Exception):

    def __init__(self, inner, *args):
        super().__init__(*args)
        self.inner = inner

class LoadError(Exception):
    pass
