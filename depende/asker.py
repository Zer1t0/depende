

ASK_OPTION_ASK = "ask"
ASK_OPTION_ALWAYS_NO = "always-no"
ASK_OPTION_ALWAYS_YES = "always-yes"

_ask_option = ASK_OPTION_ASK

def set_ask_option(option):
    global _ask_option
    _ask_option = option

def should_install_dependency(dep, is_dependency_installed):
    global _ask_option
    if not is_dependency_installed(dep):
        return True

    if _ask_option == ASK_OPTION_ALWAYS_YES:
        return True
    elif _ask_option == ASK_OPTION_ALWAYS_NO:
        return False
    else:
        result = ask_if_install(
            "'{}' is already installed. Do you want to install it again?".format(
                dep.name
            ))
        if result == "no":
            return False
        elif result == "always-no":
            _ask_option = ASK_OPTION_ALWAYS_NO
            return False
        elif result == "always-yes":
            _ask_option = ASK_OPTION_ALWAYS_YES
            return True
        elif result == "yes":
            return True
        else:
            raise ValueError("Unknown value {}".format(result))


def ask_if_install(msg):
    while True:
        answer = input("{} [y(es)/n(o)/a(ll-yes)/s(kip-all)]: ".format(msg)).lower()
        if answer in ["y", "yes"]:
            return "yes"
        elif answer in ["n", "no"]:
            return "no"
        elif answer in ["a", "all", "all-yes"]:
            return "always-yes"
        elif answer in ["s", "skip", "skip-all"]:
            return "always-no"
