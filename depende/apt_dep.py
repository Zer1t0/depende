
from .error import InstallationError
from .utils import execute_command
import requests
import os
from jinja2 import Template
from .logger import logger
from .host import host_info

APT_GPG_DIR = "/etc/apt/trusted.gpg.d/"
APT_SOURCES_DIR = "/etc/apt/sources.list.d/"

def install_apt_packages(deps, options):
    if options.update:
        _apt_update()

    packages = [dep.name for dep in deps]
    _apt_install(packages)

def install_apt_keys(deps):
    for dep in deps:
        _install_apt_key(dep)

def _apt_update():
    execute_command(["apt-get", "update"])

def _apt_install(packages):
    command = ["apt-get", "install", "-y"]
    command.extend(packages)
    execute_command(command)

def install_apt_repos(deps):
    for repo in deps:
        _install_apt_repo(repo)

def _install_apt_repo(dep):
    name = apply_jinja(dep.name)
    url = apply_jinja(dep.url)
    options = {k: apply_jinja(v) for k, v in dep.options.items()}
    components = [apply_jinja(c) for c in dep.components]

    logger.info("Add {} apt repository".format(dep.name))
    _install_apt_source(name, url, components, options)

def apply_jinja(value, **kwargs):
    j2_template = Template(value)
    kwargs["host"] = host_info()
    return j2_template.render(kwargs)


def _install_apt_key(dep):
    name = dep.name
    url = apply_jinja(dep.url)

    logger.info("Add {} gpg key".format(name))

    resp = requests.get(url)
    if resp.content.startswith(b"-----BEGIN PGP PUBLIC KEY BLOCK-----"):
        extension = "asc"
    else:
        extension = "gpg"

    filepath = os.path.join(APT_GPG_DIR, "{}.{}".format(name, extension))
    try:
        with open(filepath, "wb") as fo:
            fo.write(resp.content)
    except OSError as e:
        raise InstallationError(
            e,
            "Error writing APT key '{}': {}".format(filepath, e)
        )

def _install_apt_source(name, url, components, options):
    if options:
        options_str = []
        for k, v in options.items():
            options_str.append("{}={}".format(k, v))
        options_str = "[{}] ".format(" ".join(options_str))
    else:
        options_str = ""

    components_str = " ".join(components)

    line = "deb {}{} {}".format(options_str, url, components_str)

    filepath = os.path.join(APT_SOURCES_DIR, "{}.list".format(name))

    with open(filepath, "w") as fo:
        fo.write(line + "\n")

