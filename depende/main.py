#!/usr/bin/env python3

import argparse
import logging

from .asker import ASK_OPTION_ALWAYS_NO, ASK_OPTION_ALWAYS_YES, set_ask_option
from .load import load_dependencies
from .download_dep import install_download_deps
from .error import InstallationError, LoadError
from .apt_dep import install_apt_packages, install_apt_repos, install_apt_keys
from .utils import execute_command
from .logger import logger
from .version import __version__

def parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "file",
        help="Dependencies definition"
    )

    parser.add_argument(
        "-v", dest="verbose",
        default=0,
        help="Verbose",
        action="count"
    )

    parser.add_argument(
        "-V", "--version",
        action="version",
        version="%(prog)s {}".format(__version__),
    )


    parser.add_argument(
        "-y", "--yes",
        help="Reinstall everything again",
        action="store_true",
    )

    parser.add_argument(
        "--no",
        help="Avoid reinstalling",
        action="store_true",
    )

    args = parser.parse_args()
    return args


def main():
    args = parse_args()
    logger.setLevel(args.verbose + 1)

    if args.yes:
        set_ask_option(ASK_OPTION_ALWAYS_YES)
    elif args.no:
        set_ask_option(ASK_OPTION_ALWAYS_NO)

    try:
        dependencies = load_dependencies(args.file)
    except LoadError as e:
        logger.error(e)
        return -1

    try:
        install_dependencies(dependencies)
    except InstallationError as e:
        msg = str(e)
        if isinstance(e.inner, PermissionError):
            msg += " Maybe you need execute as root? Try sudo"
        logger.error(msg)
        return -1


def install_dependencies(deps):
    for kind in deps:
        if kind.endswith("-options"):
            continue
        for dep in deps[kind]:
            if dep.has_dependencies():
                install_dependencies(dep.dependencies)

        if kind == "python":
            install_python_packages(deps[kind])
        elif kind == "apt":
            install_apt_packages(deps[kind], deps["apt-options"])
        elif kind == "apt-repo":
            install_apt_repos(deps[kind])
        elif kind == "apt-key":
            install_apt_keys(deps[kind])
        elif kind == "download":
            install_download_deps(deps[kind])
        else:
            raise NotImplemented()


def install_python_packages(deps):
    packages = [dep.name for dep in deps]
    command = ["python3", "-m", "pip", "install"]
    command.extend(packages)
    execute_command(command)



if __name__ == '__main__':
    exit(main())

