
import platform

_host_info = {}

def host_info():
    global _host_info
    if not _host_info:
        _host_info = _get_host_info()
    return _host_info

def _get_host_info():
    return {
        "arch": _get_arch(),
        "os": _get_os_info(),
    }

def _get_os_info():
    os_release = _get_linux_os_release_values()

    return {
        "id": os_release.get("ID", ""),
        "codename": os_release.get("VERSION_CODENAME", "")
    }

def _get_arch():
    arch = platform.machine()
    if arch == "x86_64":
        return "amd64"
    else:
        return arch

def _get_linux_os_release_values():
    os_release = {}
    with open("/etc/os-release") as fi:
        lines = [l.strip() for l in fi]

    for line in lines:
        parts = line.split("=")
        name = parts[0]
        value = "=".join(parts[1:]).strip('"')
        os_release[name] = value

    return os_release
