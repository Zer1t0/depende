
import subprocess
from .error import InstallationError
from .logger import logger

def execute_command(command):
    logger.info("Execute: {}".format(" ".join(command)))
    result = subprocess.run(command)
    code = result.returncode
    if code != 0:
        raise InstallationError(
            None,
            "Installation error. Returned status: {}".format(code)
        )
