
import requests
import tempfile
import logging
from zipfile import ZipFile
import os
from tqdm import tqdm
import shutil
import stat
from urllib.parse import urlparse

from .asker import should_install_dependency
from .error import InstallationError
from .logger import logger


def install_download_deps(deps):
    for dep in deps:
        if should_install_dependency(dep, is_installed):
            install_download_dep(dep)

def is_installed(dep):
    return os.path.exists(dep.dest)

def install_download_dep(dep):
    with tempfile.TemporaryDirectory() as dirpath:
        filepath, filename = download_file_in_dir(dep.url, dirpath)

        src_file = filepath
        if dep.unzip:
            logger.info("Unzip {}".format(filename))
            src_file = unzip_file_in_dir(filepath, dirpath)

        dest_file = dep.dest
        copy_executable(src_file, dest_file)

def unzip_file_in_dir(filepath, dirpath):
    with ZipFile(filepath) as zi:
        inner_file = zi.namelist()[0]
        zi.extract(inner_file, path=dirpath)
    result_file = os.path.join(dirpath, inner_file)
    return result_file

def download_file_in_dir(url, dirpath):
    resp = requests.get(url, stream = True)
    total_size = int(resp.headers.get('content-length', 0))
    chunk_size = 16 * 1024

    filename = get_url_filename(url)
    filepath = os.path.join(dirpath, filename)
    logger.info("Downloading {}".format(filename))

    progress_bar = tqdm(total=total_size, unit='iB', unit_scale=True)
    with open(filepath, "wb") as fo:
        for chunk in resp.iter_content(chunk_size=chunk_size):
            progress_bar.update(len(chunk))
            fo.write(chunk)
    progress_bar.close()

    return (filepath, filename)

def copy_executable(src_file, dest_file):
    logger.info("Installing in {}".format(dest_file))
    try:
        shutil.copy(src_file, dest_file)
    except OSError as e:
        raise InstallationError(
            e,
            "Error copying to '{}': {}".format(
            dest_file, e
        ))

    try:
        add_executable_permissions(dest_file)
    except OSError as e:
        raise InstallationError(
            e,
            "Error givin executable permissions to '{}': {}".format(
                dest_file, e
            ))

def add_executable_permissions(path):
    os.chmod(path, stat.S_IEXEC | stat.S_IXGRP |  stat.S_IXOTH)

def get_url_filename(url):
    path = urlparse(url).path
    return os.path.basename(path)
