
import yaml
from urllib.parse import urlparse
from collections import namedtuple
from .error import LoadError

DEPENDENCY_TYPES = [
    "apt",
    "apt-options",
    "apt-key",
    "apt-repo",
    "download",
    "python"
]

class ParseError(Exception):

    def __init__(self, msg, path=None):
        super().__init__(msg)
        self.path = []
        if path:
            self.path.append(path)

    def prepend_path(self, v):
        self.path.insert(0,v)

    def __str__(self):
        return "{} : {}".format("/".join(self.path), super().__str__())


def load_dependencies(path):
    try:
        with open(path) as fi:
            dependencies = yaml.safe_load(fi)
    except OSError as e:
        raise LoadError(str(e))

    try:
        if not isinstance(dependencies, dict):
            raise ParseError(
                "Expected dictionary, got {}".format(
                    dependencies.__class__.__name__
                )
            )

        dependencies = parse_dependencies(dependencies)
    except ParseError as e:
        raise LoadError("In '{}'::{}".format(path, str(e)))

    return dependencies

def parse_dependencies(deps_obj):
    deps = {}
    if "apt-options" not in deps_obj:
        deps_obj["apt-options"] = {}

    for kind in deps_obj:
        if kind not in DEPENDENCY_TYPES:
            raise ParseError("Unknown dependency type: {}".format(kind))

        try:
            if kind == "apt-options":
                deps[kind] = parse_apt_options(deps_obj[kind])
            else:
                if not isinstance(deps_obj[kind], list):
                    raise ParseError("Expected list, got {}".format(
                        deps_obj[kind].__class__.__name__
                    ))
                deps[kind] = []
                for i, dep in enumerate(deps_obj[kind]):
                    try:
                        dep = parse_dependency(dep, kind)
                        deps[kind].append(dep)
                    except ParseError as e:
                        e.prepend_path(str(i))
                        raise e
        except ParseError as e:
            e.prepend_path(kind)
            raise e


    return deps

def parse_apt_options(options):
    update = options.get("update", True)
    if not isinstance(update, bool):
        raise ParseError("apt-options update must be bool")

    return AptOptions(update)

AptOptions = namedtuple("AptOptions", ["update"])

def parse_dependency(dep_obj, kind):
    if kind == "apt":
        return parse_apt_package(dep_obj)
    elif kind == "apt-key":
        return parse_apt_key(dep_obj)
    elif kind == "apt-repo":
        return parse_apt_repo(dep_obj)
    elif kind == "python":
        return parse_python_package(dep_obj)
    elif kind == "download":
        return parse_download_dependency(dep_obj)
    else:
        raise NotImplemented()

def parse_apt_package(dep_obj):
    if isinstance(dep_obj, str):
        return AptPackage(name=dep_obj, dependencies={})
    elif isinstance(dep_obj, dict):
        name = get_required_string(dep_obj, "name")
        deps = get_dependencies(dep_obj)
        return AptPackage(name, dependencies=deps)
    else:
        return ParseError(
            "Expected string or dictionary. Got: {}".format(dep_obj)
        )

def parse_apt_key(dep_obj):
    if not isinstance(dep_obj, dict):
        return ParseError("Expected dictionary. Got: {}".format(dep_obj))

    name = get_required_string(dep_obj, "name")
    url = get_required_string(dep_obj, "url")
    deps = get_dependencies(dep_obj)
    return AptKeyDep(name, url, dependencies=deps)


def parse_apt_repo(dep_obj):
    if not isinstance(dep_obj, dict):
        return ParseError("Expected dictionary. Got: {}".format(dep_obj))

    name = get_required_string(dep_obj, "name")
    url = get_required_string(dep_obj, "url")
    type_ = get_optional_string(dep_obj, "type", "deb")

    options = get_optional_dict_of_strings(dep_obj, "options", {})
    components = get_optional_list_of_strings(dep_obj, "components", [])
    dependencies = get_dependencies(dep_obj)

    return AptRepoDep(
        name=name,
        url=url,
        type_=type_,
        options=options,
        components=components,
        dependencies=dependencies
    )

def parse_download_dependency(dep_obj):
    if not isinstance(dep_obj, dict):
        return ParseError("Expected dictionary. Got: {}".format(dep_obj))

    name = get_required_string(dep_obj, "name")
    url_str = get_required_string(dep_obj, "url")
    dest = get_required_string(dep_obj, "dest")
    unzip = get_optional_bool(dep_obj, "unzip", False)

    url = urlparse(url_str)
    if url.scheme not in ["http", "https"]:
        raise ParseError("Unsupported url protocol: {}".format(url.scheme))

    return DownloadDependency(
        name=name,
        url=url_str,
        dest=dest,
        unzip=unzip,
        dependencies=get_dependencies(dep_obj)
    )

def parse_python_package(dep_obj):
    if isinstance(dep_obj, str):
        return PythonPackage(name=dep_obj, dependencies={})
    elif isinstance(dep_obj, dict):
        name = get_required_string(dep_obj, "name")
        deps = get_dependencies(dep_obj)
        return PythonPackage(name, dependencies=deps)
    else:
        return ParseError(
            "Expected string or dictionary. Got: {}".format(dep_obj)
        )

def get_required_string(dep_obj, attr):
    try:
        value = dep_obj[attr]
        if not isinstance(value, str):
            raise ParseError("must be a string", attr)
    except KeyError:
        raise ParseError("Expected attribute: {}".format(attr))

    return value

def get_optional_string(dep_obj, attr, default):
    value = dep_obj.get(attr, default)
    if not isinstance(value, str):
        raise ParseError("must be a string", attr)

    return value

def get_optional_bool(dep_obj, attr, default):
    value = dep_obj.get(attr, default)
    if not isinstance(value, bool):
        raise ParseError("must be boolean", attr)

    return value


def get_optional_dict_of_strings(dep_obj, attr, default):
    d = get_optional_dict(dep_obj, attr, default)
    for v in d.values():
        if not isinstance(v, str):
            raise ParseError("must be a strings dictionary", attr)
    return d

def get_dependencies(dep_obj):
    deps = get_optional_dict(dep_obj, "dependencies", {})
    return parse_dependencies(deps)

def get_optional_dict(dep_obj, attr, default):
    value = dep_obj.get(attr, default)
    if not isinstance(value, dict):
        raise ParseError("must be a dictionary", attr)

    return value

def get_optional_list_of_strings(dep_obj, attr, default):
    l = get_optional_list(dep_obj, attr, default)
    for v in l:
        if not isinstance(v, str):
            raise ParseError("must be a strings list", attr)
    return l

def get_optional_list(dep_obj, attr, default):
    value = dep_obj.get(attr, default)
    if not isinstance(value, list):
        raise ParseError("must be a list", attr)

    return value

class Dependency:

    def __init__(self, name, dependencies=None):
        self.name = name
        self.dependencies = dependencies or {}

    def has_dependencies(self):
        return bool(self.dependencies)

    def __str__(self):
        return str(self.__dict__)

    def __repr__(self):
        return repr(self.__dict__)

class PythonPackage(Dependency):
    pass

class AptPackage(Dependency):
    pass

class DownloadDependency(Dependency):

    def __init__(self, name, dependencies=None, url="", dest="", unzip=False):
        super().__init__(name, dependencies)
        self.name = name
        self.dependencies = dependencies or {}
        self.url = url
        self.dest = dest
        self.unzip = unzip

class AptRepoDep(Dependency):

    def __init__(self, name, type_, options, url, components, dependencies):
        super().__init__(name, dependencies)
        self.type_ = type_
        self.options = options
        self.url = url
        self.components = components


class AptKeyDep(Dependency):

    def __init__(self, name, url, dependencies):
        super().__init__(name, dependencies)
        self.url = url
